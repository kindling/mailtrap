<?php

/**
 * Kindling Mailtrap - Filters
 * @package Kindling_Mailtrap
 */

if (!function_exists('add_action')) {
    return;
}

add_action('kindling_ready', function () {
    add_filter('option_active_plugins', function ($plugins) {
        if (!kindling_mailtrap_enabled()) {
            return $plugins;
        }

        return array_filter($plugins, function ($plugin) {
            $toDeactivate = apply_filters('kindling_mailtrap_plugins_to_deactivate', [
                'sendgrid-email-delivery-simplified/wpsendgrid.php',
            ]);

            return !in_array($plugin, $toDeactivate);
        });
    }, PHP_INT_MAX);
});
