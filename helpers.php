<?php
/**
 * Kindling Mailtrap - Helpers
 * @package Kindling_Mailtrap
 */

/**
  * Checks if image forwarding is enabled
  *
  * @return boolean
  */
function kindling_mailtrap_enabled()
{
    $enabled = false;
    if (function_exists('kindling_development_is_localhost')) {
        $enabled = kindling_development_is_localhost();
    }

    return (bool) apply_filters('kindling_mailtrap_enabled', $enabled);
}
