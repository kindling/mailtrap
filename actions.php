<?php

/**
 * Kindling Mailtrap - Actions
 * @package Kindling_Mailtrap
 */

if (!function_exists('add_action')) {
    return;
}

add_action('kindling_ready', function () {
    /**
     * Initializes Mailtrap.
     */
    add_action('phpmailer_init', function ($phpmailer) {
        if (!kindling_mailtrap_enabled()) {
            return;
        }

        $phpmailer->isSMTP();
        $phpmailer->Host = 'smtp.mailtrap.io';
        $phpmailer->SMTPAuth = true;
        $phpmailer->Port = 2525;
        $phpmailer->Username = apply_filters('kindling_mailtrap_username', '880181517f0848');
        $phpmailer->Password = apply_filters('kindling_mailtrap_password', '29e075977f9655');
    });
});
